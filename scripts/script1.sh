

mkdir res/genome
cd  res/genome
wget -O ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -k ecoli.fasta.gz

cd ..
cd ..

mkdir -p out/fastqc
mkdir -p log/cutadapt
mkdir -p out/cutadapt
mkdir -p res/genome/star_index
mkdir -p out/multiqc
mkdir -p out/multiqc/multiqc_data
mkdir -p out/star
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate \
--genomeDir res/genome/star_index/ \
--genomeFastaFiles res/genome/ecoli.fasta \
--genomeSAindexNbases 9

for sampleid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq)
	do
	
	bash scripts/script2.sh $sampleid
        done
multiqc -o out/multiqc/home/vant/testsim
